default:
  just --list

install:
  docker compose exec rabbit-mq rabbitmqctl add_vhost test
  docker compose exec rabbit-mq rabbitmqctl add_user test local
  docker compose exec rabbit-mq rabbitmqctl set_permissions -p test test ".*" ".*" ".*"