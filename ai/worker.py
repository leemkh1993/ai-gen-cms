import pika
import time
import sys
import os

cal_job_queue = 'calculate_job'
generate_job_queue = 'generate_job'

class Worker:
  
  def __init__(self):
    self.conn = pika.BlockingConnection(pika.ConnectionParameters(
      host = '127.0.0.1',
      port = 37800,
      virtual_host = 'test',
      credentials=pika.PlainCredentials('test', 'local')
    ))
    print('Worker created')
    
  def run(self):
    channel = self.conn.channel()
    channel.queue_declare(queue=cal_job_queue)
    channel.queue_declare(queue=generate_job_queue)
    
    channel.basic_consume(
      queue=cal_job_queue,
      on_message_callback=self.handleCalculateJob,
      auto_ack=True
    )
    
    channel.basic_consume(
      queue=generate_job_queue,
      on_message_callback=self.handleGenerateJob,
      auto_ack=True
    )
    
    print('Worker started')
  
  def __del__(self):
    self.conn.close()
    print('Worker destroyed')

  def handleCalculateJob(self, job):
    print('Received calculate job:', job)
    self.calculate()
    
  def handleGenerateJob(self, job):
    print('Received generate job:', job)
    self.inference()
    
  def calculate(self):
    print('Calculating...')
    
  def inference(self):
    print('Inferencing...')
    


def main():
  worker = Worker()
  
  worker.inference()
  
  # print('[*] Waiting for jobs. To exit press CTRL+C')
  # while True:
  #   worker.run()
  #   time.sleep(1)
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)