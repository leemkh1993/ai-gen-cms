# Install

## AI
```bash
conda create -n ai python=3.11
conda activate ai
cd ai
pip install -r requirements.txt
```

## CMS
```bash
cd cms
yarn install
```
---
# High level architecture
```mermaid
flowchart TD
  user --> CMS --> DB
  CMS --> oec[On Event Create] --> ic{Params: kickStart?} -- YES --> queue
  CMS --> opu[On Post Update] --> rg{Params: Regenerate?} -- YES --> queue
  ic -- NO --> NULL
  rg -- NO --> NULL
  worker -- 1. PULL cal job --> queue
  worker --> cal[Calculate number of posts] -- 2. Send gen job --> queue[Queue]
  aiWorker[AI] -- 3. PULL gen job --> queue
  aiWorker -- Async --> gc[Generate Content] -- 4. CALL API --> cp[Create Post] -- Complete note --> discord
  
  discord[Discord]
```
---
# Low level architecture

```mermaid
---
title: AI workflow
---
flowchart TD
  subgraph CMS
    event[Event]
    post[Post]
  end

  r[Return]

  event -- PUSH --> qeu
  event -- PUSH --> puw

  subgraph puw[Post Update Worker]
    rg[Is Regen?] -- YES --> e[Event]
    e -- GET --> opuEd[Event Description] -- Post process --> opuPed[Proceed Event Description]
    opuPed -- FORM --> opuJob[Job]
  end

  opuJob -- PUSH --> qgc
  rg -- NO --> r

  subgraph euw[Event Update Worker]
    iks{{Is kickStart?}} -- YES --> euwEd[Event Description] -- Post process --> ped[Proceed Event Description] --> jobs[Jobs]
    iks -- YES --> rfi[Release Frequency + Interval + Event date] --> np[Number of post] --> jobs
  end

  iks -- NO --> r
  jobs -- PUSH --> qgc

  subgraph qs[Queues]
    qeu[ai-content-queue-event-update]
    qgc[ai-content-queue-gen-content]
  end

  qeu -- Trigger --> euw
  qgc -- Trigger --> gcw


  subgraph gcw[Gen Content Worker]
    job[Job] -- Get --> wed[Event Description]
    wed -- Post process --> wped[Proceed Event Description]
    wped -- Add --> p[Prompt]
    p -- Send --> ai[AI Model] -- Generate --> cp[Content]
    cp -- Call API Create --> post[Post]
    post -- Complete --> discord[Discord]
  end
```
---
# AWS Architecture
```mermaid
flowchart TD
  users[Users] 

  users --> igw

  subgraph aws["AWS (VPC)"]
    subgraph pub[Public Subnet]
      igw[Internet Gateway] --> lb[L7 Load balancer]

      lb --> cms

      subgraph Lambda
        gc[ai-content-worker-gen-content]
        eu[ai-content-worker-event-update]
      end

      gc -- CRUD --> cms
      eu -- PUSH --> qgc
      
      subgraph ECS
        cms[ai-content-queue-cms]
      end

      cms -- PUSH --> qeu
      cms -- UPLOAD --> media
      cms -- Store Data --> db

      subgraph SQS
        qeu[ai-content-queue-event-update]
        qgc[ai-content-queue-gen-content]
      end

      qeu -- Trigger --> eu
      qgc -- Trigger --> gc

      subgraph S3
        media[ai-content-media]
      end

      subgraph RDS
        db[ai-content-db]
      end
    end
  end

  subgraph Discord
    t[Thread]
  end

  cms -- On Event CRUD --> t
  cms -- On Post CRUD --> t
```
---
## Job Schema
```json
{
  "event_id": 123,
  "post_id": 456, // nullable -> Create post if null
}
```