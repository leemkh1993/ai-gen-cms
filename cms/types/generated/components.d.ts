import type { Schema, Attribute } from '@strapi/strapi';

export interface SliceSlice extends Schema.Component {
  collectionName: 'components_slice_slices';
  info: {
    displayName: 'slice';
    icon: 'slideshow';
    description: '';
  };
  attributes: {
    image: Attribute.Media<'images' | 'files' | 'videos' | 'audios'>;
    content: Attribute.Text;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'slice.slice': SliceSlice;
    }
  }
}
