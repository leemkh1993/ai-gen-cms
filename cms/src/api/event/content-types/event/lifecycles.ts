import schema from './schema.json';
import { ApiEventEvent } from '@/types/generated/contentTypes'

const sendJobs = async (event) => {
  const result = event.result;

  if (!result.start_post_generation) {
    return;
  }

  console.log(event, result)
  console.log('afterUpdate')
};

export default {
  beforeCreate: async (event) => {
    const { data, where, select, populate } = event.params;
    console.log('beforeCreate')
  },
  afterCreate: sendJobs,
  afterUpdate: sendJobs
};
